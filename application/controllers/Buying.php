<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Buying extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library(array('form_validation'));
		$this->load->helper(array('buying_rules','string'));
    $this->load->model(array('Category','Books','Shopping','User'));
  }

  public function shopping()
  {
    if ($this->session->userdata('is_logged')) {
      $iduser = $this->session->userdata('iduser');
      $infouser = $this->User->GetInfoUserForIdUser($iduser);
      $pricesend = 0;
      if($infouser != null){
        $pricesend = $infouser->PriceSend;
      }
      $books = $this->GetBooks($iduser);
      $vista = $this->load->view('Users/shopping',array(
        'books' => $books, 'pricesend' => $pricesend),TRUE);
    } else {
      $vista = $this->load->view('Users/login','',TRUE);
    }
    $this->getTemplate($vista);
  }

  public function GetBooks($iduser)
  {
    $idbooks = $this->Shopping->GetBooksInShoppingCart($iduser);
    $books = array();
    foreach ($idbooks as $idbook) {
      $id = $idbook->IdBook;
      $book = $this->Books->FindBookForId($id);
      array_push($books,$book);
    }
    return $books;
  }

  public function deleteBookInShoppingCar(){
    if ($this->session->userdata('is_logged')) {
      $idbook = $this->input->post('IdBook');
      $iduser = $this->session->userdata('iduser');
      $books = $this->GetBooks($iduser);
      $infouser = $this->User->GetInfoUserForIdUser($iduser);
      $pricesend = 0;
      if($infouser != null){
        $pricesend = $infouser->PriceSend;
      }
      if ($this->Shopping->deleteBookForShoppingCar($idbook)) {
        $numbooks = $this->Shopping->CountBooksInTheCar($this->session->userdata('iduser'));
        if ($numbooks==0) {
          $books = $this->Books->SuggestBooks();
          $vista = $this->load->view('Books/show_books',array('books' => $books),TRUE); 
        } else {
          $vista = $this->load->view('Users/shopping',array(
            'books' => $books, 'pricesend' => $pricesend),TRUE);
        }
      } else {
        $vista = $this->load->view('Users/shopping',array(
          'books' => $books,'error' => 'No se pudo eliminar el libro del carrito'),TRUE);    
      }
    } else {
      $vista = $this->load->view('Users/login','',TRUE);
    }
    $this->getTemplate($vista);
  }

  public function buy()
  { 
    if($this->session->userdata('is_logged')){
      $idbook = $this->input->post('IdBook');
      $iduser = $this->session->userdata('iduser');
      if (!$this->Shopping->ExistCardInPayCard($iduser)){
        $vista = $this->load->view('Buy/payment',array(
          'idbook' => $idbook),TRUE);
      } else {
        $book = $this->Books->FindBookForId($idbook);
        $infouser = $this->User->GetInfoUserForIdUser($iduser);
        $pricesend = $infouser->PriceSend;
        $vista = $this->load->view('Buy/buycompleted',array(
          'book' => $book,'pricesend' => $pricesend,'type' => 'Card'),TRUE);
      }
    }else {
      $vista = $this->load->view('Users/login','',TRUE);
    }
    $this->getTemplate($vista);
  }

  public function buyTheShoppingCar()
  {
    if($this->session->userdata('is_logged')){
      $total = $this->input->post('Total');
      $iduser = $this->session->userdata('iduser');
      $infouser = $this->User->GetInfoUserForIdUser($iduser);
      $address = 'Dirección: '.$infouser->Dir.' Alcaldia: '.$infouser->Alcaldia.' CP: '.$infouser->CP;
      if (!$this->Shopping->ExistCardInPayCard($iduser)){
        $books = $this->Books->SuggestBooks();
        $vista = $this->load->view('Books/show_books',array(
          'books' => $books,
          'error' => 'Las Compras Multiples solo son con Tarjeta, Realiza una Compra Simple y registra una Tarjeta Para usar este Servicio'),TRUE);
      } else {
        if(isset($_POST['IdBook'])){
          foreach ($_POST['IdBook'] as $idbook) {
            $card = $this->Shopping->CardForIdUserAndIdBook($iduser,$idbook);
            $idpay = $card->IdPayCard;
            $sold = array(
              'TotalPrice' => $total,
              'Address_Send' => $address,
              'Type_Transaction' => 'Card',
              'IdUser' => $iduser,
              'IdBook' => $idbook,
              'IdPayCard' => $idpay,
            );
            $this->Shopping->AddSold($sold);
            $this->Shopping->deleteBookForShoppingCar($idbook);
          }
        }
        $books = $this->Books->SuggestBooks();
        $vista = $this->load->view('Books/show_books',array(
          'books' => $books,'msg' => 'Compra Realizada'),TRUE);
      }
    }else {
      $vista = $this->load->view('Users/login','',TRUE);
    }
    $this->getTemplate($vista);
  }



  public function ChangeBuyPay($idbook)
  {
    if($this->session->userdata('is_logged')){
      $vista = $this->load->view('Buy/payment',array('idbook' => $idbook),TRUE);
    }else {
      $vista = $this->load->view('Users/login','',TRUE);
    }
    $this->getTemplate($vista);
  }

  public function buyCompleted()
  {
    if ($this->session->userdata('is_logged')) {
      $idbook = $this->input->post('IdBook');
      $iduser = $this->session->userdata('iduser');
      $namecard = $this->input->post('namecard');
      $numbercard = $this->input->post('numbercard');
      $month = $this->input->post('month');
      $year = $this->input->post('year');
      $this->form_validation->set_rules(getBuyingRules());
      if ($this->form_validation->run() == FALSE) {
        $vista = $this->load->view('Buy/payment',array(
          'idbook' => $idbook,'msg' => 'Verifique los campos'),TRUE);
      }else {
        $datacard = array(
          'NameCard' => $namecard,
          'NumberCard' =>$numbercard,
          'Month' => $month,
          'Year' => $year,
          'IdUser' => $iduser,
        );
        if (!$this->Shopping->AddCard($datacard)) {
          $vista = $this->load->view('Buy/payment',array(
            'idbook' => $idbook,'msg' => 'No se pudo ingresar los datos.'),TRUE);
        }else{
          $book = $this->Books->FindBookForId($idbook);
          $infouser = $this->User->GetInfoUserForIdUser($iduser);
          $pricesend = $infouser->PriceSend;
          $vista = $this->load->view('Buy/buycompleted',array(
            'book' => $book,'pricesend' => $pricesend,'type' => 'Card'),TRUE);
        }
      }
    } else {
      $vista = $this->load->view('Users/login','',TRUE);
    }
    $this->getTemplate($vista);
  }

  public function buyed()
  {
    $books = $this->Books->SuggestBooks();
    $vista = $this->load->view('Books/show_books',array(
      'books' => $books,'msg' => 'Compra Realizada'),TRUE);
    $this->getTemplate($vista);
  }

  public function buyedNoCard($idbook,$type)
  {
    if ($this->session->userdata('is_logged')) {
      $book = $this->Books->FindBookForId($idbook);
      $infouser = $this->User->GetInfoUserForIdUser($this->session->userdata('iduser'));
      $pricesend = $infouser->PriceSend;
      $vista = $this->load->view('Buy/buycompleted',array(
        'book' => $book,'pricesend' => $pricesend,'type' => $type),TRUE);
    } else {
      $vista = $this->load->view('Users/login','',TRUE);
    }
    $this->getTemplate($vista);
  }

  public function FinishBuy()
  {
    if ($this->session->userdata('is_logged')) {
      $idbook = $this->input->post('IdBook');
      $total = $this->input->post('Total');
      $type = $this->input->post('Type');
      $iduser = $this->session->userdata('iduser');
      $infouser = $this->User->GetInfoUserForIdUser($iduser);
      $address = 'Dirección: '.$infouser->Dir.' Alcaldia: '.$infouser->Alcaldia.' CP: '.$infouser->CP;
      if ($type=='Bank'||$type=='Cash') {
        $idpay = 0;
      } else {
        $card = $this->Shopping->CardForIdUserAndIdBook($iduser,$idbook);
        $idpay = $card->IdPayCard;
      }
      $sold = array(
        'TotalPrice' => $total,
        'Address_Send' => $address,
        'Type_Transaction' => $type,
        'IdUser' => $iduser,
        'IdBook' => $idbook,
        'IdPayCard' => $idpay,
      );
      if (!$this->Shopping->AddSold($sold)) {
        $vista = $this->load->view('Buy/buycompleted',array(
          'book' => $book,'pricesend' => $$infouser->PriceSend,'type' => $type,
          'error' => 'No se pudo realizar la compra, intente más tarde'
        ),TRUE);
      }else{
        $books = $this->Books->SuggestBooks();
        $vista = $this->load->view('Books/show_books',array(
          'books' => $books,'msg' => 'Compra Realizada'),TRUE);
      }
    } else {
      $vista = $this->load->view('Users/login','',TRUE);
    }
    $this->getTemplate($vista);
  }

  // Método Template que Carga todos los elemento de las Vistas
  public function getTemplate($view)
  {
    $title = 'Buying'; // titulo del Encabezado
    $name='Ingresa';
    $is_online=false;
    $data = $this->Category->GetCategory();
    $numbooks = 0;
    $is_admin = false;
    if ($this->session->userdata('is_logged')) {
      $isuser = $this->session->userdata('iduser');
      $name = $this->session->userdata('name');
      $is_online=true;
      $numbooks = $this->Shopping->CountBooksInTheCar($this->session->userdata('iduser'));
      if ($this->session->userdata('type') =='Admin') {
        $is_admin = true;
      }
    }
    // Partes de la vista 
    $allviews = array(
      'head' => $this->load->view('Layout/head',array('title' => $title),TRUE), // Encabezado
      'nav' => $this->load->view('Layout/nav',array(
        'category' => $data,'numbooks' => $numbooks,'name' => $name,'is_online' => $is_online
      ),TRUE),
      'content' => $view, // Contenido de la pagina
      'footer' => $this->load->view('Layout/footer',array(
        'is_online' => $is_online,'is_admin' => $is_admin),TRUE) // Pie de pagina
    );
    $this->load->view('home',$allviews); // mandamos todo al home
  }
}