<!-- Barra Superior del Dashboard -->	
</head>
<body>
<header class="section-header">
	<section class="header-main">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-3">
					<div class="brand-wrap">
						<h2 class="logo-text">
							<a class="nav-link text-dark" href="<?=base_url('Home')?>">
								<img class="logo" src="<?= base_url('assets/images/cloud.png') ?>" width="50" height="50"> Web Site
							</a>
						</h2>
					</div> <!-- brand-wrap.// -->
				</div>
				<div class="col-lg-6 col-sm-6">
					<form action="<?=base_url('Home/search')?>" class="search-wrap" method="POST">
						<div class="input-group">
							<input type="text" name="search" class="form-control" placeholder="Buscar por Titulo, Autor, Editorial, ISBN">
							<div class="input-group-append">
								<button class="btn btn-outline-secondary" type="submit">
									<img class="logo" src="<?= base_url('assets/images/search.png') ?>" width="20" height="20">
								</button>
							</div>
						</div>
					</form> <!-- search-wrap .end// -->
				</div> <!-- col.// -->
				<div class="col-lg-3 col-sm-8">
					<div class="widgets-wrap d-flex justify-content-end">
						<div class="widget-header">
							<a href="<?=base_url('Buying/shopping')?>" class="ml">
								<div class="container">
									<img class="logo" src="<?= base_url('assets/images/shopping-cart.png') ?>" width="64" height="64"><hr>
									<p>
										<small class="text-secondary">Carrito</small>
										<span class="text-success"><?= $numbooks ?></span>
									</p>
								</div>
							</a>
						</div> <!-- widget .// -->
						<?php 
							if($is_online){
								echo "<div class=\"widget-header ml\">";
									echo "<a href=".base_url('Home/logout')." class=\"ml\">";
										echo "<div class=\"container\">";
											echo "<img class=\"logo\" src=".base_url('assets/images/login2.png')." width=\"60\" height=\"60\"><hr>";
											echo "<span class=\"text-primary\">".$name."</span>";
											echo "<p class=\"text-success\"><small>Online</small></p>";
										echo "</div>";
									echo "</a>";
								echo "</div>";
							} else {
								echo "<div class=\"widget-header ml\">";
									echo "<a href=".base_url('Home/login')." class=\"ml\">";
										echo "<div class=\"container\">";
											echo "<img class=\"logo\" src=".base_url('assets/images/login2.png')." width=\"60\" height=\"60\"><hr>";
											echo "<span class=\"text-secondary\">".$name."</span>";
											echo "<p class=\"text-secondary\"><small>Offline</small></p>";
										echo "</div>";
									echo "</a>";
								echo "</div>";
							}	
						?>
					</div>	<!-- widgets-wrap.// -->	
				</div> <!-- col.// -->
			</div> <!-- row.// -->
		</div> <!-- container.// -->
	</section> <!-- header-main .// -->

	<nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
		<div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="main_nav">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link pl-0" href="<?=base_url('Home/category')?>"> <strong>
							<img class="logo" src="<?= base_url('assets/images/books.png') ?>" width="20" height="20"> Libros</strong>
						</a>
					</li>
					<?php foreach($category as $i => $item): ?>
						<?php if($i <= 6): ?>
							<li class="nav-item">
								<a class="nav-link" href="<?= base_url('Home/searchCategory/').$item->IdCategory ?>"><?= $item->Name ?></a>
							</li>
						<?php endif; ?>
					<?php endforeach; ?>
					<?php if(count($category) > 7): ?>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mas...</a>
							<div class="dropdown-menu">
								<?php foreach($category as $i => $item): ?>
									<?php if($i > 6): ?>
										<a class="dropdown-item" href="<?= base_url('Home/searchCategory/').$item->IdCategory ?>"><?= $item->Name ?></a>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						</li>
					<?php endif; ?>
				</ul>
			</div> <!-- collapse .// -->
		</div> <!-- container .// -->
	</nav>
</header> <!-- section-header.// -->