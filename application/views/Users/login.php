<?php
	if (isset($error)) {
		echo "<div class=\"alert alert-danger\" role=\"alert\">".$error."</div>";
	}
?>
<!-- Ajustamos la Posicion de los Elementos en un Contenedor -->
<div class="container">
	<form action="<?= base_url('Home/AccessAccount') ?>" method="POST">
		<h1>Acceso a la Cuenta</h1>
				<!-- Creamos una Tarjeta de Presentacion -->
				<div class="card">
					<!-- Seccion donde va el Link Limpieza  -->
					<div class="card-header bg-success mb-3">
						<ul class="nav nav-pills card-header-pills">
							<li class="nav-item">
								<a class="nav-link text-white" href="login">Limpiar</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-white" href="<?=base_url('Home/register')?>">¿No estas registrado?, Registrate!</a>
							</li>
						</ul>
					</div>
					<!-- Seccion del Cuerpo de la Tarjeta -->
					<div class="card-body">
						<div class="form-group">
							<label>Correo</label>
							<input type="email" name="email" class="form-control" placeholder="Escriba su email" value="<?= set_value('email') ?>">
							<div class="text-danger"><?= form_error('email') ?></div>
						</div>
						<!-- Input de Contraseña -->
						<div class="form-group">
							<label>Contraseña</label>
							<input type="password" name="pass" class="form-control" placeholder="Ingrese su contraseña"> 
							<div class="text-danger"><?= form_error('pass') ?></div>
						</div>
						<!-- Boton Ingresar -->
						<div class="form-group text-center">
							<button type="submit" class="btn btn-success">Ingresar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<br>
</div>