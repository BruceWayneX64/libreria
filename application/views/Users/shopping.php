<section class="section-content bg padding-y">
	<?php
		if (isset($msg)) {
			echo "<div class=\"alert alert-success\" role=\"alert\">".$msg."</div>";
		}
		if (isset($error)) {
			echo "<div class=\"alert alert-danger\" role=\"alert\">".$error."</div>";
		}
	?>
	<div class="container">
		<?php if(!empty($books)): ?>
			<div class="row">
				<main class="col-sm-9">
					<?php 
						$res = 0;
					?>
					<?php foreach($books as $book): ?>	
						<div class="card">
							<table class="table table-hover shopping-cart-wrap">
								<thead class="text-muted">
									<tr>
										<th scope="col">Libro</th>
										<th scope="col" width="120">Precio</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<figure class="media">
												<div class="col	">
													<img src="<?= base_url($book[0]->DirImage) ?>" width="150" height="200">
													<p><h6 class="title"><?= $book[0]->Title ?></h6></p>
												</div>
												<figcaption class="media-body">
													<dl class="dlist-inline small">
														<dt>Autor:</dt>
														<dd><?= $book[0]->Author ?></dd>
													</dl>
													<dl class="dlist-inline small">
														<dt>Editorial: </dt>
														<dd><?= $book[0]->Editorial ?></dd>
													</dl>
												</figcaption>
											</figure> 
										</td>
										<td> 
											<div class="price-wrap"> 
												<var class="price">$ <?= $book[0]->Price ?><var> 
												<small class="text-muted">(Pesos MX)</small>
												<hr>
												<form action="<?= base_url('Buying/deleteBookInShoppingCar')?>" method="POST">
													<input type="hidden" name="IdBook" value="<?= $book[0]->IdBook ?>">
													<?php if(count($books)<=1): ?>
														<button type="submit" class="btn btn-outline-danger">Cancelar Compra</button>
													<?php else: ?>
														<button type="submit" class="btn btn-outline-danger">Eliminar Compra</button>
													<?php endif ?>
												</form>
											</div> <!-- price-wrap .// -->
										</td>
									</tr>
								</tbody>
							</table>
							<?php $res=$res+$book[0]->Price; ?>
						</div> <!-- card.// -->
					<?php endforeach ?>
				</main> <!-- col.// -->
				<aside class="col-sm-3">
					<dl class="dlist-align">
						<dt>Precio Total: </dt>
						<dd class="text-right">$ <?= $res  ?></dd>
					</dl>
					<dl class="dlist-align">
						<dt>Envio:</dt>
						<dd class="text-right">$ <?= $pricesend ?></dd>
					</dl>
					<dl class="dlist-align h4">
						<dt>Total: <p><small>(+ iva)</small></p> </dt>
						<dd class="text-right"><strong>$<?= $res=($res+$pricesend)*1.16 ?></strong></dd>
					</dl>
					<hr>
					<figure class="itemside mb-3">
						<form action="<?= base_url('Buying/buyTheShoppingCar') ?>" method="POST">
							<?php
								foreach ($books as $book) {
									echo "<input type=\"hidden\" name=\"IdBook[]\" value=".$book[0]->IdBook.">";
								}
							?>
							<input type="hidden" name="Total" value="<?= $res ?>">
							<button type="submit" class="btn btn-outline-success">Finalizar Compra</button>
						</form>
					</figure>
				</aside> <!-- col.// -->
			</div>
			<?php else: ?>
				<h1>No hay elementos En el Carrito</h1>
				<br><br><br><br><br><br><br>
		<?php endif ?>
	</div> <!-- container .//  -->
</section>
<br>