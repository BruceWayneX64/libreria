<div class="container">
<?php
		if (isset($msg)) {
			echo "<div class=\"alert alert-success\" role=\"alert\">".$msg."</div>";
		}
		if (isset($error)) {
			echo "<div class=\"alert alert-danger\" role=\"alert\">".$error."</div>";
		}
	?>
	<?php if(!empty($foundbooks)): ?>
		<h1>Buscando: <?= $search ?></h1>
		<br>
		<?php foreach($foundbooks as $book): ?>	
			<div class="row">
				<div class="col">
					<div class="card border-secondary rounded-0" >
						<div class="card-body">
							<div class="row">
								<div class="img-wrap">
									<img src="<?= base_url($book->DirImage) ?>" width="150" height="200">
								</div>
								<article class="col-sm-6">
									<h4 class="title"><?= $book->Title ?></h4>
									<small><?= $book->Description ?></small>
									<dl class="dlist-align">
										<dt>Autor</dt>
										<dd><?= $book->Author ?></dd>
									</dl>  <!-- item-property-hor .// -->
									<dl class="dlist-align">
										<dt>Editorial</dt>
										<dd><?= $book->Editorial ?></dd>
									</dl>  <!-- item-property-hor .// -->
									<dl class="dlist-align">
										<dt>Año de Publicación</dt>
										<dd><?= $book->Year ?></dd>
									</dl>  <!-- item-property-hor .// -->
								</article> <!-- col.// -->
								<aside class="col-sm-3 border-left">
									<div class="action-wrap">
										<div class="price-wrap h4">
											<span class="price">Precio: <?= $book->Price ?></span>
										</div> <!-- info-price-detail // -->
										<p>
											<form action="<?= base_url('Buying/buy')?>" method="POST">
											<input type="hidden" name="search" value="<?= $search ?>">											
												<input type="hidden" name="IdBook" value="<?= $book->IdBook ?>">
												<button type="submit" class="btn btn-primary">Comprar</button>
											</form>
										</p>
											<form action="<?= base_url('Home/AddBooktoCar') ?>" method="POST">
												<input type="hidden" name="search" value="<?= $search ?>">
												<input type="hidden" name="IdBook" value="<?= $book->IdBook ?>">
												<button type="submit" class="btn btn-link">Añadir al carrito</button>
											</form>
										<br><br><hr>
										<dl class="dlist-align">
											<dt>ISBN</dt>
											<dd><?= $book->ISBN ?></dd>
										</dl>  <!-- item-property-hor .// -->
										<dl class="dlist-align">
											<dt>Formato</dt>
											<dd><?= $book->Format ?></dd>
										</dl>  <!-- item-property-hor .// -->
									</div> <!-- action-wrap.// -->
								</aside> <!-- col.// -->
							</div> <!-- row.// -->
						</div> <!-- card-body .// -->
					</div> <!-- card .// -->
					<br>
				</div> <!-- col.// -->
			</div> <!-- row.// -->
		<?php endforeach ?>
	<?php else: ?>
		<h1>No Encontró ningún libro referente a <?= $search ?></h1>
		<br><br><br><br><br><br><br>
	<?php endif ?>
</div>