<?php
// Verifica si la Funcion del helper existe 
if(!function_exists('getBuyingRules')){
	function getBuyingRules(){
		return array(
			array(
				'field' => 'namecard',	// Nombre del Identificador 
        'label' => 'Nombre de la Tarjeta', // nombre de la etiqueta 
        'rules' => 'required', // reglas separadas por un pipe |
        'errors' => array(
					'required' => 'El %s es requerido.', // manda error si no hay datos encontrados
				)
      ),
      array(
				'field' => 'numbercard',
      	'label' => 'Numeros de la Tarjeta',
        'rules' => 'required|is_natural_no_zero|max_length[16]',
        'errors' => array(
					'required' => 'Los %s son requeridos.', // manda error si no hay datos encontrados
					'is_natural_no_zero' => 'Los %s no son Validos.', // manda error si el dato no es un numero natural
          'max_length' => 'Los %s excedieron la cantidad de digitos permitidos.',
				)
			),
			array(
				'field' => 'month',
				'label' => 'Mes',
				'rules' => 'required',
				'errors' => array(
						'required' => 'El %s es requerido.', // manda error si el dato no es un numero natural
				)
			),
			array(
				'field' => 'year',
				'label' => 'Año',
				'rules' => 'required',
				'errors' => array(
						'required' => 'El %s es requerido.', // manda error si el dato no es un numero natural
				)
			),     
    );
  }
}
?>